﻿using PolyAndCode.UI;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.Networking;

public class CellEntry : MonoBehaviour, ICell
{
    public Text rankT;
    public Image thumbnailImg;
    public Text nameT;
    public Text pointsT;

    private int currRank;
    private CachedData cached;
    private IEnumerator getImage;

    public void Awake()
    {
        cached = CachedData.Instance;
        getImage = GettingImage("", 0);
    }
    public void Set(PlayerEntry _entry)
    {
        rankT.text = _entry.rank.ToString();
        nameT.text = _entry.name;
        pointsT.text = _entry.points.ToString();
        currRank = _entry.rank;
        if (cached.avatars.ContainsKey(_entry.rank))
        {
            thumbnailImg.sprite = cached.avatars[_entry.rank];
        }
        else
        {
            thumbnailImg.sprite = null;
            StopCoroutine(getImage);
            getImage = GettingImage(_entry.avatarUrl, _entry.rank);
            StartCoroutine(getImage);
        }
    }

    private IEnumerator GettingImage(string _url, int _cacheNum)
    {
        UnityWebRequest request = UnityWebRequestTexture.GetTexture(_url);
        yield return request.SendWebRequest();
        if (request.isNetworkError || request.isHttpError)
            Debug.Log(request.error);
        else
        {
            Texture2D webTexture = ((DownloadHandlerTexture)request.downloadHandler).texture as Texture2D;
            Sprite webSprite = SpriteFromTexture2D(webTexture);
            cached.avatars.Add(_cacheNum, webSprite);
            thumbnailImg.sprite = webSprite;
        }
    }
    private Sprite SpriteFromTexture2D(Texture2D texture)
    {
        return Sprite.Create(texture, new Rect(0.0f, 0.0f, texture.width, texture.height), new Vector2(0.5f, 0.5f), 100.0f);
    }
}
