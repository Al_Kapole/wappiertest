﻿using System.Collections.Generic;
using UnityEngine;
using PolyAndCode.UI;

public class MyRecyclableScroller : MonoBehaviour, IRecyclableScrollRectDataSource
{
    [SerializeField]
    private RecyclableScrollRect _recyclableScrollRect;
    
    private List<PlayerEntry> entries = new List<PlayerEntry>();

    internal delegate void LoadData(List<PlayerEntry> _entries);
    internal LoadData loadData;

    internal int bottomEntry { get { return _recyclableScrollRect.bottomEntry; } }

    private void Awake()
    {
        loadData = FirstLoadData;
    }

    public void Reset()
    {
        loadData = Refresh;
    }

    private void Refresh(List<PlayerEntry> _entries)
    {
        entries = _entries;
        _recyclableScrollRect.ReloadData(this);
        //After refresh, start calling reload.
        loadData = ReloadData;
    }
    
    private void FirstLoadData(List<PlayerEntry> _entries)
    {
        entries = _entries;
        _recyclableScrollRect.Initialize(this);
        //After first initialization, start calling reload.
        loadData = ReloadData;
    }
    private void ReloadData(List<PlayerEntry> _entries)
    {
        entries = _entries;
    }

    public int GetItemCount()
    {
        return entries.Count;
    }
    
    public void SetCell(ICell cell, int index)
    {
        CellEntry item = cell as CellEntry;
        item.Set(entries[index]);
    }
}