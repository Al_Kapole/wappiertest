﻿using UnityEngine;
using System.Collections.Generic;

public class CachedData
{
    private static CachedData _instance;
    public static CachedData Instance
    {
        get
        {
            if (_instance == null)
            {
                _instance = new CachedData();
                _instance.avatars = new Dictionary<int, Sprite>();
            }
            return _instance;
        }
    }

    public Dictionary<int, Sprite> avatars;

    public void ClearCachedAvatars()
    {
        foreach(KeyValuePair<int, Sprite> avatar in avatars)
            Object.Destroy(avatar.Value);
        avatars = new Dictionary<int, Sprite>();
    }
}
