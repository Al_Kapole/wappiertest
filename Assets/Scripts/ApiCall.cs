﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine.Events;
using UnityEngine.Networking;

public class ApiCall
{
    public IEnumerator Call(string _url, Dictionary<string, string> _headers, 
        UnityAction<string> _onSuccess, UnityAction<string> _onFail)
    {
        UnityWebRequest www = UnityWebRequest.Get(_url);

        if (_headers != null)
        {
            foreach (KeyValuePair<string, string> header in _headers)
                www.SetRequestHeader(header.Key, header.Value);
        }
        yield return www.SendWebRequest();
        if (www.isNetworkError || www.isHttpError)
            _onFail(www.error);
        else
            _onSuccess(www.downloadHandler.text);
    }

    public static string Authenticate(string username, string password)
    {
        string auth = username + ":" + password;
        auth = System.Convert.ToBase64String(System.Text.Encoding.GetEncoding("ISO-8859-1").GetBytes(auth));
        auth = "Basic " + auth;
        return auth;
    }
}
