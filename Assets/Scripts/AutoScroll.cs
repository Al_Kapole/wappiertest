﻿using UnityEngine;
using UnityEngine.UI;
/*
 *Add this to an object with a scroll rect component to auto scroll it.
 */
[RequireComponent(typeof(ScrollRect))]
public class AutoScroll : MonoBehaviour
{
    [SerializeField]
    private float yVelocity;
    private ScrollRect scrollR;

    void Start()
    {
        scrollR = GetComponent<ScrollRect>();
    }
    
    void Update()
    {
        scrollR.velocity = new Vector2(0, yVelocity);
    }
}
