﻿public struct PlayerEntry
{
    public PlayerEntry(int _rank, string _name, int _points, string _avatarUrl)
    {
        rank = _rank;
        name = _name;
        points = _points;
        avatarUrl = _avatarUrl;
    }
    public int rank;
    public string name;
    public int points;
    public string avatarUrl;
}
