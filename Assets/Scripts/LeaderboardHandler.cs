﻿using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using LitJson;

public class LeaderboardHandler : MonoBehaviour
{
    [SerializeField]
    private ScrollRect scrollR;
    [SerializeField]
    private string username = "test_me";
    [SerializeField]
    private string password = "G00dw1LL";
    [SerializeField]
    [Tooltip("How many cells before the last one should it call for next page of leaderboard.")]
    [Range(1,50)]
    private int nextPageAtLast = 10;

    private MyRecyclableScroller mrs;
    private string url = "https://test.wappier.com/api/leaderboard/";
    private int page = 0;
    private ApiCall apiCall;
    private Dictionary<string, string> headers = new Dictionary<string, string>();
    private List<PlayerEntry> entries;
    private bool isLoading;
    private CachedData cached;
    private UnityEngine.Events.UnityAction<string> onSuccess;

    void Start()
    {
        cached = CachedData.Instance;
        entries = new List<PlayerEntry>();
        apiCall = new ApiCall();
        headers.Add("Authorization", ApiCall.Authenticate(username, password));
        scrollR.onValueChanged.AddListener(OnScroll);
        mrs = scrollR.GetComponent<MyRecyclableScroller>();
        onSuccess = OnSuccess;
        GetLeaderboard();
    }

    public void Refresh()
    {
        cached.ClearCachedAvatars();
        entries = new List<PlayerEntry>();
        page = 0;
        mrs.Reset();
        onSuccess = OnSuccessFrameDelay;
        GetLeaderboard();
    }

    private void OnScroll(Vector2 _value)
    {
        if (mrs.bottomEntry > entries.Count - nextPageAtLast && !isLoading)//if it is near the bottom
        {
            GetLeaderboard();
        }
    }
    

    private void GetLeaderboard()
    {
        isLoading = true;
        StartCoroutine(apiCall.Call(url + page, headers, onSuccess, OnFail));
    }

    private void OnSuccess(string _jdata)
    {
        Debug.Log("GetLeaderboard Success");
        JsonData data = JsonMapper.ToObject(_jdata);
        Save(data);
        mrs.loadData(entries);
        isLoading = false;
        page++;
    }
    private void OnFail(string _error)
    {
        Debug.LogError("GetLeaderboard Failed: " + _error);
        isLoading = false;
        scrollR.onValueChanged.RemoveListener(OnScroll);
    }

    //On Refresh only
    private void OnSuccessFrameDelay(string _jdata)
    {
        Debug.Log("GetLeaderboard with Frame Delay Success");
        scrollR.onValueChanged.RemoveListener(OnScroll);
        JsonData data = JsonMapper.ToObject(_jdata);
        Save(data);
        mrs.loadData(entries);
        StartCoroutine(Ready());
    }
    private System.Collections.IEnumerator Ready()
    {
        yield return null;
        isLoading = false;
        page++;
        scrollR.onValueChanged.AddListener(OnScroll);
        onSuccess = OnSuccess;
    }
    //----------------

    private void Save(JsonData _data)
    {
        int length = _data.Count;
        for (int i = 0; i < length; i++)
        {
            JsonData cellData = _data[i];
            entries.Add(new PlayerEntry(entries.Count+1, cellData["name"].ToString(), 
                (int)cellData["points"], cellData["avatar"].ToString()));
        }
    }
}
